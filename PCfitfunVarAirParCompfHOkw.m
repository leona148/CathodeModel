function out= PCfitfunVarAirParCompfHOkw(in,data,datag,pressvals,massvals,parcomp)
if exist('parcomp','var')
else
    parcomp=0;
end
kvalsall=[];

% Choose variables to fit.
% Suggested variables:
% in.ioeff, in.CL.kw, in.CL.fH(1), in.Rext, in.CL.kieff, in.CL.keeff, in.alpha
x0g=[in.ioeff, in.alpha, in.CL.fH(1), in.U, in.CL.kw];%
out.x0g=x0g;

% Initial guesses are all ones because variables normalized to to the guess
x0=ones(1,length(x0g));

% Bounding variables by factor of ten. Ultimately they  will be bounded by
% zero and infinity because we run the fit multiple times, but we don't
% want to stray to far from the guess values because we want the relative 
% step size to be similar for each parameter
lb=ones(1,length(x0)).*0.1;
ub=ones(1,length(x0)).*10;


% Options
optionsf1=optimset('TolFun',1e-10,'TolX',1e-3,...
    'MaxFunEvals',1e5,'MaxIter',1e3,'DiffMinChange',1e-4,'DiffMaxChange',1e-1);

% Run lsqnonlin
[kvect,resnorm,residual,exitflag,output,lambda,jacobian] =lsqnonlin(@fitfun,x0,lb,ub,optionsf1);
xg=kvect.*x0g;

fitquality=max([kvect(:);1./kvect(:)]);
%if one of the fitted parameters was far from the guess values, then the data
% should be renormalized and the program will continue to fit. This is so
% that the relative step size remains similar for all parameters
while fitquality > 1.5  
    % Run lsqnonlin again after renormalizing (second run)
    for ii=1:length(xg)
        sprintf('%0.4e',xg(ii))
    end %for ii
    x0g=xg;
    resnorm
    out.x0g2=x0g;

    [kvect,resnorm,residual,exitflag,output,lambda,jacobian] =lsqnonlin(@fitfun,x0,lb,ub,optionsf1);
    xg=kvect.*x0g;
    
    fitquality=max([kvect(:);1./kvect(:)]);
end % while fitquality > 1.5

% Run lsqnonlin again after renormalizing (final run)
x0g=xg;
out.x0gf=x0g;
% now change the minimum step size to get finer solution (1e-6)
optionsf1=optimset('TolFun',1e-10,'TolX',1e-3,...
    'MaxFunEvals',1e5,'MaxIter',1e3,'DiffMinChange',1e-6,'DiffMaxChange',1e-1);

[kvect,resnorm,residual,exitflag,output,lambda,jacobian] =lsqnonlin(@fitfun,x0,lb,ub,optionsf1);
xg=kvect.*x0g;

% Print Fitted Values
for ii=1:length(xg)
    sprintf('%0.4e',xg(ii))
end %for ii

% Collect output structure
out.kvect=kvect;
out.xg=xg;
out.resnorm=resnorm;
out.residual=residual;
out.exitflag=exitflag;
out.residual=residual;
out.output=output;
out.lambda=lambda;
out.jacobian=jacobian;

%% Functions

    function res=fitfun(kvals)
        res=[];
        rest=[];
        vm=[];
        im=[];
        vom=[];
        iom=[];
        
        numvars=length(kvals);
        
        
        xg=kvals.*x0g;
        if parcomp==false %if we aren't using parallel computing we will be
            % interested in how the fit is fairing while it runs
            for ii=1:length(xg)
                sprintf('%0.4e',xg(ii))
            end %for ii
            
        end %if
        
        for mm=1:length(massvals);
            
            for pp=1:length(pressvals);
                if parcomp==false %if not using par. comp.
                    for vv=1:size(data(mm).ix,1);
                        ini=inputs2B(1,0.45,pressvals(pp),massvals(mm));
                        %ini is a special input for this specific iteration
                        %of the for loop and cannot be used outside of the
                        %parfor loop
                        %[in.ioeff, in.alpha, in.CL.fH(1), in.CL.kw];
                        ini.ioeff=xg(1);
                        ini.alpha=xg(2);
                        ini.CL.fH=xg(3).*ones(size(ini.CL.fH));
                        ini.U=xg(4);
                        ini.CL.kw=xg(5);
                        
                        % potential value
                        ini.pot=data(mm).vx(vv,pp);
                        
                        % Run Function with BVP solver (fast version)
                        solii=PEMFC_NDBVKn(ini,datag(mm).v(vv,pp).solair);
                        datapp(vv).solair=solii;
                        
                    end %for vv
                else %if using par. comp.
                    parfor vv=1:size(data(mm).ix,1); % this is the parallel for loop
                        ini=inputs2B(1,0.45,pressvals(pp),massvals(mm));
                        %ini is a special input for this specific iteration
                        %of the for loop and cannot be used outside of the
                        %parfor loop
                        %[in.ioeff, in.alpha, in.CL.fH(1), in.CL.kw];
                        ini.ioeff=xg(1);
                        ini.alpha=xg(2);
                        ini.CL.fH=xg(3).*ones(size(ini.CL.fH));
                        ini.U=xg(4);
                        ini.CL.kw=xg(5);
                        
                        % potential value
                        ini.pot=data(mm).vx(vv,pp);
                        
                        % Run Function with BVP solver
                        solii=PEMFC_NDBVKn(ini,datag(mm).v(vv,pp).solair);
                        datapp(vv).solair=solii;
                    end %parfor vv
                end %if not using par. comp.
                datat(mm,pp).data=datapp;
            end  %for pp
        end %for mm
        
        % Now that we have run the model using either single node mode or
        % parallel computing mode we will organize that data and calculate
        % residuals
        for mm=1:length(massvals);
            for pp=1:length(pressvals);
                for vv=1:size(data(mm).ix,1);
                    % save output structure for future guesses
                    datag(mm).v(vv,pp).solair=datat(mm,pp).data(vv).solair;
                    % Save voltage and current data
                    vm(vv,pp,mm)=data(mm).vx(vv,pp);
                    im(vv,pp,mm)=datag(mm).v(vv,pp).solair.I;
                    
                    % Calculate Residual
                    resvv=((datag(mm).v(vv,pp).solair.I-data(mm).ix(vv,pp))/max(data(mm).ix(:,pp)));
                    
                    res=[res resvv];
                    
                    if vm(vv,pp,mm)<0.3 % we only have one point at 0.3 V so we
                        res=[res resvv]; % will double it to weight all potentials
                    end %if                 equally
                end %for vv
                
            end  %pp
            if parcomp==false%if we aren't using parallel computing we will be
                % interested in how the fit is fairing while it runs so we will
                % plot the fit
                figure(2)
                subplot(2,2,mm)
                plot2=semilogx(data(mm).ix,data(mm).vx,'o',im(:,:,mm),vm(:,:,mm),':x');
                xlim([0 inf])
            end %if not using par. comp.
        end %for mm
        %resg=(xg-x0gi)./(xg+x0gi);
        
        res=[res(:)];%;resg(:).*1e-1];
        
        kvalsall=[kvalsall;kvals,sum(res.^2)];
        if parcomp==false%if we aren't using parallel computing we will be
            % interested in how the fit is fairing while it runs so we will
            % plot the values as they change
            figure(1)
            plot1=plot(kvalsall);
            xlim([0 inf])
            drawnow
        end %if not using par. comp.
        
        res=[res(:)];
    end %fitfun



end %fun



