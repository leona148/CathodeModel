% four subrouties
run1=0; % Model Comparison Figs 5-6
run2=0; % Air Sensitivity Fig 7
run3=0; % Oxygen Sensivitiy Fig 8
run4=1; % Optimization Fig 9

%Model Results for Figures in Paper (Figs. 5-9)
figii=0;
%% Polarization Curves
if run1
    tic
    load('MEAdata/Gen2BLot0813varload.mat')
    
    data(1).V=data2.V;
    data(2).V=data3.V;
    data(3).V=data4.V;
    data(1).I=data2.I;
    data(2).I=data3.I;
    data(3).I=data4.I;
    mvals=2:4;
    
    
    psig=[2 12 30];
    pps=1:3;
    pvals=psig(pps)./14.7+0.83;
    
    %P = 30          12         2   psig
    in=inputs2B;
    outnd0=PEMFC_NDBVKn(in);
    
    vvals=[0.85:-0.01:0.8,0.78:-0.02:0.2];
    
    for mm=1:length(mvals)
        
        for pp=1:length(pvals)
            
            outnd=outnd0;
            in=inputs2B(1,0.45,pvals(pp),mvals(mm));
            po(pp)=in.xo.*in.ptot;
            
            for vv=1:length(vvals)
                in=inputs2B(1,0.45,pvals(pp),mvals(mm));
                in.pot=vvals(vv);
                %in.fig=1;
                outnd=PEMFC_NDBVKn(in,outnd);
                toc
                dataout(mm).v(vv,pp).solair=outnd;
                imod(mm).v(vv,pp)=dataout(mm).v(vv,pp).solair.I;
                vmod(mm).v(vv,pp)=vvals(vv);
            end %for vv
            Rmod(mm).v(:,pp)=diff(vmod(mm).v(:,pp))./diff(imod(mm).v(:,pp));
            Rfinmod(mm,pp)=Rmod(mm).v(length(vvals)-1,pp);
            Rfintheo(mm,pp)=in.Rlim;
            Imax(mm,pp)=max(data(mm).I(:,pp));
        end %for pp
    end %for mm
    %%
    for pp=1:3
        figii=figii+1;
        figure(figii)
        figlist(figii)={'Polarization with Model'};
        subplot(1,2,1)
        
        plot(data(1).I(:,pp),data(1).V(:,pp),'d',...
            data(2).I(:,pp),data(2).V(:,pp),'o',...
            data(mm).I(:,pp),data(mm).V(:,pp),'s')
        hold on
        plot(imod(1).v(:,pp),vmod(1).v(:,pp),...
            imod(2).v(:,pp),vmod(2).v(:,pp),...
            imod(mm).v(:,pp),vmod(mm).v(:,pp))
        hold off
        legend('2 mg/cm^2','3 mg/cm^2','4 mg/cm^2')
        
        xlim([0 2])
        xlabel('Current Density  / A cm^-^2','FontSize',12)
        ylabel('Potential / V','FontSize',12)
        title('Comparison with Experimental Results','FontSize',12)
        
        subplot(1,2,2)
        semilogx(data(1).I(:,pp),data(1).V(:,pp),'d',...
            data(2).I(:,pp),data(2).V(:,pp),'o',...
            data(mm).I(:,pp),data(mm).V(:,pp),'s')
        hold on
        plot(imod(1).v(:,pp),vmod(1).v(:,pp),...
            imod(2).v(:,pp),vmod(2).v(:,pp),...
            imod(mm).v(:,pp),vmod(mm).v(:,pp))
        hold off
        xlim([1e-3 2])
        xlabel('Current Density  / A cm^-^2','FontSize',12)
        ylabel('Potential / V','FontSize',12)
        title('(Gen2B Lot 0630 LM150-OX50)','FontSize',12)
    end %for pp
    IgorExp=[data(1).V,data(1).I,data(2).V,data(2).I,...
        data(mm).V,data(mm).I];
    IgorMod=[vmod(1).v,imod(1).v,vmod(2).v,imod(2).v,...
        vmod(mm).v,imod(mm).v];
    save('Results/PolarizationRaw.mat','data','dataout','IgorExp','IgorMod')
    save('Results/PolarizationResults.mat','IgorExp','IgorMod')
    toc
    
    
    
    %%  Catalyst Layer Profiles
    tic
    
    if exist('dataout','var')
    else
        load('PolarizationGuess.mat');
    end
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Profile'};
    
    piter=3;
    mm=2;
    viter=round(interp1(vvals,[1:length(vvals)],[0.8 0.5 0.1]));
    if isnan(viter(3))
        viter(3)=length(vvals);
    end %if
    
    vasym=[0:0.1:0.6];
    
    xvals1=dataout(mm).v(viter(1),piter).solair.NDsol.x;
    xvals2=dataout(mm).v(viter(2),piter).solair.NDsol.x;
    xvals3=dataout(mm).v(viter(3),piter).solair.NDsol.x;
    xdiff1=xvals1(1:length(xvals1)-1)+diff(xvals1)./2;
    xdiff2=xvals2(1:length(xvals2)-1)+diff(xvals2)./2;
    xdiff3=xvals3(1:length(xvals3)-1)+diff(xvals3)./2;
    
    
    asym=max(imod(mm).v(:,piter))-(vasym-min(vmod(mm).v(:,piter)))./Rfintheo(mm,piter)-0.05;
    currentprofiles1=1-(dataout(mm).v(viter(1),piter).solair.NDsol.y(:,3))./max(abs(dataout(mm).v(viter(1),piter).solair.NDsol.y(:,3)));
    currentprofiles2=1-(dataout(mm).v(viter(2),piter).solair.NDsol.y(:,3))./max(abs(dataout(mm).v(viter(2),piter).solair.NDsol.y(:,3)));
    currentprofiles3=1-(dataout(mm).v(viter(3),piter).solair.NDsol.y(:,3))./max(abs(dataout(mm).v(viter(3),piter).solair.NDsol.y(:,3)));
    
    currentdistr1=diff(currentprofiles1)./diff(dataout(mm).v(viter(1),piter).solair.NDsol.x);
    currentdistr2=diff(currentprofiles2)./diff(dataout(mm).v(viter(2),piter).solair.NDsol.x);
    currentdistr3=diff(currentprofiles3)./diff(dataout(mm).v(viter(3),piter).solair.NDsol.x);
    
    sat1=dataout(mm).v(viter(1),piter).solair.NDsol.y(:,11);
    sat2=dataout(mm).v(viter(2),piter).solair.NDsol.y(:,11);
    sat3=dataout(mm).v(viter(3),piter).solair.NDsol.y(:,11);
    
    satfHO1=(dataout(mm).v(viter(1),piter).solair.NDsol.y(:,11)-(1-in.CL.fH(1)))./in.CL.fH(1);
    satfHO2=(dataout(mm).v(viter(2),piter).solair.NDsol.y(:,11)-(1-in.CL.fH(1)))./in.CL.fH(1);
    satfHO3=(dataout(mm).v(viter(3),piter).solair.NDsol.y(:,11)-(1-in.CL.fH(1)))./in.CL.fH(1);
    
    subplot(2,2,1)
    plot(xdiff1,currentdistr1,xdiff2,currentdistr2,xdiff3,currentdistr3)
    xlabel('Position in Catalyst Layer','FontSize',12)
    ylabel('Current Distribution (dI/dx*L/I','FontSize',12)
    xlim([0 1])
    hold off
    
    subplot(2,2,2)
    plot(xvals1,sat1,...
        xvals2,sat2,...
        xvals3,sat3)
    xlim([0 2])
    ylim([0 1])
    xlabel('Position in Catalyst Layer','FontSize',12)
    ylabel('Saturation','FontSize',12)
    
    subplot(2,2,3)
    plot(xvals1,abs(dataout(mm).v(viter(1),piter).solair.NDsol.y(:,8)),...
        xvals2,abs(dataout(mm).v(viter(2),piter).solair.NDsol.y(:,8)),...
        xvals3,abs(dataout(mm).v(viter(3),piter).solair.NDsol.y(:,8)))
    xlabel('Position in Catalyst Layer','FontSize',12)
    ylabel('Oxygen Fraction','FontSize',12)
    xlim([0 1])
    hold off
    
    subplot(2,2,4)
    plot(imod(mm).v(:,piter),vmod(mm).v(:,piter),...
        imod(mm).v(viter,piter),vmod(mm).v(viter,piter),'+',...
        asym,vasym)
    xlim([0 inf])
    xlabel('Current Density  / A cm^-^2','FontSize',12)
    ylabel('Potential / V','FontSize',12)
    
    
    IgorProfile08=[dataout(mm).v(viter(1),piter).solair.NDsol.x(:),currentprofiles1,...
        abs(dataout(mm).v(viter(1),piter).solair.NDsol.y(:,8)),satfHO1];
    IgorProfile05=[dataout(mm).v(viter(2),piter).solair.NDsol.x(:),currentprofiles2,...
        abs(dataout(mm).v(viter(2),piter).solair.NDsol.y(:,8)),satfHO2];
    IgorProfile01=[dataout(mm).v(viter(3),piter).solair.NDsol.x(:),currentprofiles3,...
        abs(dataout(mm).v(viter(3),piter).solair.NDsol.y(:,8)),satfHO3];
    
    IgorProfile08d=[xdiff1(:),currentdistr1(:)];
    IgorProfile05d=[xdiff2(:),currentdistr2(:)];
    IgorProfile01d=[xdiff3(:),currentdistr3(:)];
    
    save('Results/ProfilesResults.mat','IgorProfile08','IgorProfile05','IgorProfile01',...
        'IgorProfile08d','IgorProfile05d','IgorProfile01d')
    toc
    
end %if run1
%% Sensitivity (Set-up)
tic

in=inputs2B(1,0.45);
in.fig=0;
inair=in;
in=inputs2B(0,0.45);
in.fig=0;
ino2=in;

Va=0.85:-0.02:0.0;
fp=1.005;
fm=2-fp;
var=[];
var(1).name='in.CL.fH';
var(1).title='Hydrophobic Pore Fraction';
var(1).val=[in.CL.fH(1).*fm in.CL.fH(1) in.CL.fH(1).*fp];

var(2).name='in.CL.rho';
var(2).title='Catalyst Layer Porosity';
var(2).val=[in.CL.rho.*fm in.CL.rho in.CL.rho.*fp];

var(3).name='in.CL.kieff';
var(3).title='Ionic Conductivity / S cm^-^1';
var(3).val=[in.CL.kieff.*fm in.CL.kieff in.CL.kieff.*fp];

var(4).name='in.CL.keeff';
var(4).title='Electric Conductivity / S cm^-^1';
var(4).val=[in.CL.keeff.*fm in.CL.keeff in.CL.keeff.*fp];

var(5).name='in.CL.kw';
var(5).title='Permeability / cm^2';
var(5).val=[in.CL.kw.*fm in.CL.kw in.CL.kw.*fp];

var(6).name='in.Rext';
var(6).title='External Resistance';
var(6).val=[in.Rext.*fm in.Rext in.Rext.*fp];

var(7).name='in.ioeff';
var(7).title='Exchange Current Density';
var(7).val=[in.ioeff.*fm in.ioeff in.ioeff.*fp];

var(8).name='in.CL.L';
var(8).title='Catalyst Loading';
var(8).val=[in.CL.L.*fm in.CL.L in.CL.L.*fp];

var(9).name='in.CL.theta';
var(9).title='Catalyst Loading';
var(9).val=acos([cos(in.CL.theta).*fm cos(in.CL.theta) cos(in.CL.theta).*fp]);

%% Air Sensitivity

if run2
    in=inair;
    outnd=PEMFC_NDBVKn(in);
    tic
    for vv=1:length(Va)
        in=inair;
        in.pot=Va(vv);
        outnd=PEMFC_NDBVKn(in,outnd);
        outnd0=outnd;
        for vii=1:length(var)
            for ii=1:3
                in=inair;
                eval([var(vii).name,' = ones(size(',var(vii).name,')).*',num2str(var(vii).val(ii)),';',]);
                in.pot=Va(vv);
                in.CL.vfs=(inair.CL.vfs+inair.CL.rho)-in.CL.rho;
                
                if ii==2
                    outnd=outnd0;
                else
                    in.pot=Va(vv);
                    outnd=PEMFC_NDBVKn(in,outnd);
                end %if
                toc
                
                Iv(vv).I(vii,ii)=outnd.I;
                mod(vii).I(vv,ii)=outnd.I;
                mod(vii).V(vv,ii)=Va(vv);
                if ii>2
                    senair(vv,vii)=(Iv(vv).I(vii,3)-Iv(vv).I(vii,1))./Iv(vv).I(vii,2)./(fp-fm);
                end %if ii
                
            end %for ii
            
        end %for vii
        
    end %for vv
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Air Sensitivity'};
    
    plot(Va,(senair(:,:)))
    legend(var(1).title,var(2).title,var(3).title,var(4).title,...
        var(5).title,var(6).title,var(7).title,var(8).title,var(9).title)
    ylim([-1 1])
    ylabel('Sensitivity')
    xlabel('Potential / V')
    hold off
    Igorsenair=[Va(:),(senair(:,:))];
    save('Results/AirSensRaw.mat','var','mod','Va','Igorsenair','inair')
    save('Results/AirSensResults.mat','Igorsenair')
    toc
end %if run2
%% Oxygen Sensitivity (6 min run time)

if run3
    tic
    in=inputs2B(0,0.45,1);
    in.fig=0;
    in0=in;
    outnd=PEMFC_NDBVKn(in);
    
    for vv=1:length(Va)
        vv
        in=ino2;
        in.pot=Va(vv);
        outnd=PEMFC_NDBVKn(in,outnd);
        outnd0=outnd;
        for vii=1:length(var)
            for ii=1:3
                in=ino2;
                eval([var(vii).name,' = ones(size(',var(vii).name,')).*',num2str(var(vii).val(ii)),';',]);
                in.CL.vfs=(ino2.CL.vfs+ino2.CL.rho)-in.CL.rho;

                if ii==2
                    outnd=outnd0;
                else
                    in.pot=Va(vv);
                    outnd=PEMFC_NDBVKn(in,outnd);
                end %if
                toc
                Iv(vv).I(vii,ii)=outnd.I;
                mod(vii).I(vv,ii)=outnd.I;
                mod(vii).V(vv,ii)=Va(vv);
                if ii>2
                    seno2(vv,vii)=(Iv(vv).I(vii,3)-Iv(vv).I(vii,1))./Iv(vv).I(vii,2)./(fp-fm);
                end %if ii
                
            end %for ii
            
        end %for vv
        
    end %for vii
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Oxygen Sensitivity'};
    plot(Va,(seno2(:,:)))
    legend(var(1).title,var(2).title,var(3).title,var(4).title,...
        var(5).title,var(6).title,var(7).title,var(8).title,var(9).title)
    ylim([-1 1])
    ylabel('Sensitivity')
    xlabel('Potential / V')
    hold off
    Igorseno2=[Va(:),(seno2(:,:))];
    save('Results/O2SensRaw.mat','var','mod','Va','Igorseno2','ino2')
    save('Results/O2SensResults.mat','Igorseno2')
    toc
end %if run3
%% Loading/Density Optimization (~1 hour run time) (program runs 2100 times)

if run4
    tic
    in=inputs2B(1,0.45);
    pval=in.ptot;
    outnd0=PEMFC_NDBVKn(in);
    % these are the loadings, densities, and potentials that will be used
    % in the optimization
    loadings=[5:-0.1:0.2];
    density=[0.4:-0.02:0.2];
    Vs=[0.8:-0.02:0.4];
    for ii=1:length(loadings)
        for jj=1:length(density)
            in=inputs2B(1,0.45,pval,loadings(ii),density(jj));
            % we will assume tortuosity does not change with density so
            % that conductivity scales with density
            in.CL.kieff=in.CL.kieff.*density(jj)./0.32;
            in.CL.keeff=in.CL.keeff.*density(jj)./0.32;
            
            outnd=outnd0;
            loadmat(ii,jj)=loadings(ii);
            gasketmat(ii,jj)=(in.CL.L+in.GDL.L)./0.0025.*1.05;
            denmat(ii,jj)=density(jj);
            for vv=1:length(Vs)
                in.pot=Vs(vv);
                outnd=PEMFC_NDBVKn(in,outnd);%outstruct(ii,jj).v(vv).out
                toc
                outstruct(ii,jj).v(vv).out=outnd;
                modopt(vv).I(ii,jj)=outnd.I;
                modopt(vv).V(ii,jj)=Vs(vv);
                modopt(vv).P(ii,jj)=modopt(vv).I(ii,jj).*modopt(vv).V(ii,jj);
                
                
            end %for vv
            outnd0=outstruct(ii,jj).v(1).out;
            
        end %for jj
        
    end %for ii
    save('Results/OptRaw.mat','modopt','outstruct','Vs','loadings','density',...
        'loadmat','denmat','in')
    toc
    
    %% Calculations from Model Results
    tic
    
    iif=length(loadings);
    jjf=length(density);
    IgorLoadPot=ones(length(loadings),length(Vs)+1);
    IgorLoadPot(:,1)=loadings(:);
    % These calculations calculate which iteration of the optimization is
    % closest to experimental values
    mvals=2:4;
    ExpCondii=round(interp1(loadings,[1:length(loadings)],mvals));
    ExpCondjj=round(interp1(density,[1:length(density)],0.32));
    densityest=density(ExpCondjj);
    loadingest=loadings(ExpCondii);
    
    for vv=1:length(Vs)
        IgorLoadPot(:,vv+1)=modopt(vv).I(:,ExpCondjj);
        
        % in order to find the apex of the surface we are finding where the
        % gradient is zero
        % first calculate gradient:
        [FX,FY] = gradient(modopt(vv).I,-0.5,-0.02);
        modopt(vv).gradL=FX;
        modopt(vv).gradD=FY;
        % find closest point to maximum:
        maxI=max(max(modopt(vv).I));
        [ival jval]=find(modopt(vv).I==maxI);
        % interpolate gradient to zero:
        % bounds of interpolation:
        ivali=ival-1;
        if ivali<=0
            ivali=1;
        end %if
        ivalf=ival+1;
        if ivali>size(modopt(vv).I,1)
            ivali=size(modopt(vv).I,1);
        end %if
        jvali=jval-1;
        if jvali<=0
            jvali=1;
        end %if
        jvalf=jval+1;
        if jvali>size(modopt(vv).I,2)
            jvali=size(modopt(vv).I,2);
        end %if
        % interpolation:
        loadopt(vv)=interp1(modopt(vv).gradD(ivali:ivalf,jval),...
            loadings(ivali:ivalf),0);
        denopt(vv)=interp1(modopt(vv).gradL(ival,jvali:jvalf),...
            density(jvali:jvalf),0);
        % graph the gradient to check interpolation:
        figure(1)
        subplot(2,1,1)
        plot(modopt(vv).gradD(:,jval),loadings,'-o',[0],loadopt(vv),'x')
        subplot(2,1,2)
        plot(modopt(vv).gradL(ival,:),density,'-o',[0],denopt(vv),'x')
    end %for vv
    
    % calculate which mod structures will represent the potentials of
    % interest
    vvs3=round(interp1(Vs,[1:length(Vs)],[0.8 0.65 0.40]));
    vv80=vvs3(1); 
    vv65=vvs3(2); 
    vv40=vvs3(3); 
    
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Loading Opt. Current'};
    
    plot(Vs,loadopt)
    ylabel('Loading / mg cm^-^2')
    xlabel('Potential / V')
    xlim([0 inf])
    ylim([0 inf])
    
    figii=figii+1;
    figure(figii)
    plot(Vs,denopt)
    ylabel('Density / g cm^-^3')
    xlabel('Potential / V')
    xlim([0 inf])
    ylim([0 inf])
    
    figii=figii+1;
    figure(figii)
    subplot(1,2,1)
    surfl(loadmat,denmat,modopt(vv80).I)
    zlabel('Current / A cm^-^2')
    xlabel('Loading / mg cm^-^2')
    ylabel('Density / g_c_a_t cm^-^3')
    zlim([0 inf])
    xlim([0 inf])
    ylim([0 inf])
    
    subplot(1,2,2)
    surfl(loadmat,denmat,modopt(vv40).I)
    zlabel('Current / A cm^-^2')
    xlabel('Loading / mg cm^-^2')
    ylabel('Density / g_c_a_t cm^-^3')
    zlim([0 inf])
    xlim([0 inf])
    ylim([0 inf])
    
    IgorOpt=[Vs(:),denopt(:),loadopt(:)];
    Igorxyz80=[loadmat(:),denmat(:),modopt(vv80).I(:)];
    Igorxyz65=[loadmat(:),denmat(:),modopt(vv65).I(:)];
    Igorxyz40=[loadmat(:),denmat(:),modopt(vv40).I(:)];
    
    save('Results/OptResults.mat','IgorOpt','Igorxyz80','Igorxyz65','Igorxyz40',...
        'IgorLoadPot')
    toc
end %if run4