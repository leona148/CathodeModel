function in=inputs2B(Air,naf,ptot,masscat,density,Tcell,Tdew)

if exist('Air','var')
else
    Air=true;
end

if exist('naf','var')
else
    naf=0.45;
end

if exist('ptot','var')
    in.ptot=ptot;
else
    in.ptot=3;
end

if exist('masscat','var')
else
    masscat=4;
end

if exist('density','var')
else
    density=0.32;
end

if exist('Tcell','var')
    in.T=Tcell;
else
    in.T=353;
end

if exist('Tdew','var')
    in.Tdew=Tdew;
else
    in.Tdew=353;
end
                   % K
%% General Model Characteristics

in.fig=0;

% Constants/Conditions
in.F=96500;                 % C/mole (Faraday's Constant)
in.n=4;

in.R=8.314;                 % J/mol/K
in.V0=18*1;                 %[=]g/mol*cc/g, molar volume of water
in.p0vap=exp(11.6832-3816.44/(in.T-46.13));%[=]atm Vapor Pressure H2O [1]
in.p0vapinlet=exp(11.6832-3816.44/(in.Tdew-46.13));%[=]atm Vapor Pressure H2O [1]
in.MW=[18 32 28].*1e-3;%kg/mol
if Air
    in.xw=in.p0vapinlet./in.ptot;
    in.xo=(1-in.xw).*0.21;
    in.xn=1-in.xo-in.xw;
else
    in.xw=in.p0vapinlet./in.ptot;
    in.xo=1-in.xw;
    in.xn=0;  
end 
in.Vg=in.R*in.T/in.ptot*10; %[=]J/mol/atm *10 atm*cc/J=cc/mol, molar volume gas
in.po0=in.ptot*in.xo;
in.pot=0.6;                 % Cell Potential / V

% Kinetic Parameters
in.U=0.8528;                	% Redox Potential V +/-0.0050
in.B=in.R.*in.T./in.F;           % Tafel Slope V/decade
in.ioeff=1.3001e3;          % Fitted  +/- 512.4
in.kmat=100;              	% mol/atm/s/cc evaporation rate constant
in.alpha=0.4160;%         % Fitted  +/- 0.0098

% Transport Parameters
in.mu=(2695.3-6.6*in.T)*1e-11;%[=]bar*s, water viscosity [1]
in.Dow=0.3022*(in.T/323.83)^2.334;%Diffusion of O2 in H2O [=] bar cm2/s [2]
in.Don=0.0544*(in.T/143.01)^1.823;%Diffusion of N2 in O2  [=] bar cm2/s [2]
in.Dwn=0.2599*(in.T/299.42)^2.334;%Diffusion of air in H2O  [=] bar cm2/s [2]
in.Rext0=0.046;                      % External Resistance Ohm-cm2 
% Hydrophobicity Properties
in.gamma=0.12398-0.00017393*in.T; %N/m

%% Catalyst Layer Characteristics
% BET area = 685 m2/g
CL.masscat=masscat; % mg/cm2
CL.den=density.*1e3;
CL.L=CL.masscat./CL.den;  %cm (1.25 mil/mg)
CL.den=CL.masscat./CL.L; % mg/cc Pressed density  

CL.massn=(CL.masscat*naf)/(1-naf);
CL.sVn=0.0005;  % cc/mg
CL.Ln=CL.massn.*CL.sVn; % cm
CL.Vn=CL.massn.*CL.sVn; % cc/cm2

% Pellets           #3      #5      #1      for reference    Bulk CL mat'l
% MIP results: for: 0.222   0.304   0.336   gcat/cc pellets  0.222
% Specific Volumes: 4.50    3.29    2.98    cc/gcat pellet   4.50
%                   1.49    1.55    1.37    cc/gcat pore     1.13
%                   0.41    0.41    0.41    cc/gcat nafion   0.41
% that leaves:
%                   2.60    1.33   1.20     cc/gcat cat.     2.96
CL.sVs=0.00171;  % ccs/mg 1.71 +/-0.77
CL.Vs=CL.masscat.*CL.sVs; % cc/cm2
CL.Vp=CL.L-CL.Vs-CL.Vn;
CL.porosity=CL.Vp./CL.masscat.*1e3;

% Volume Fractions
CL.vfn=CL.Vn./CL.L;
CL.vfp=CL.Vp./CL.L;
CL.vfs=CL.Vs./CL.L;


% Pore properties
CL.rho=CL.Vp./CL.L;%0.8;  % porosity (void fraction)
CL.ro=[0.113 0.084 0.0041];%     % characteristic pore radius (um) from MIP
CL.sr=[0.42 1.24 0.22];%    % characteristic spread of pore size distribution
CL.fk=[0.29 0.45 0.26];%   % fraction of total pore distribution
% Hydrophobicity Properties
CL.theta=90.02.*pi/180;      %105 from Weber et al ref. 3
%   because capillary pressure is always >= 0
CL.fHOg=CL.vfn./(1-CL.rho);  % 0.1934 +/- 0.0702
CL.fH=0.2873.*ones(size(CL.ro));           % 0.2873 +/- 0.1097 



% Transport Parameters:
CL.Dcat=2.*sum(CL.ro.*CL.fk).*(1./CL.rho-1).*1e-4;% Est. avg. particle Dia. /cm
CL.kw0=CL.Dcat.^2.*CL.rho.^3./(150.*(1-CL.rho).^2); %from Kozeny-Carmen ref. 7
CL.kw=5.0254e-13;%[=]cm2, saturated permiability       %+/-2.4136e-12    
% Electronic conductivity S/cm % fitted
CL.keeff=0.62;%
CL.Re=CL.L./CL.keeff;
% Ionic conductivity S/cm 
CL.ki0=exp(-1.84e4./in.R.*(1./in.T-1./243))./in.T; % Ref.5 based on 260nm cast thin film
%CL.ki0=0.5*0.39.^1.5*exp(15000/in.R*(1/303.15-1/in.T))./1e0; %Ref 1. for thick
%   Nafion
CL.kieff=CL.ki0.*CL.vfn.^(3/2); %   Ref.5 based on 260nm thick cast thin film and bruggeman relation
%   fitted
CL.kieff=18.1e-03;       % from Impedance Fit 0.0181 +/- 0.0049
CL.Ri=CL.L./CL.kieff;


%% Membrane Characteristics
Mem.L=0.0025;               % Thickness cm
Mem.ksatm=2e-14;           % saturated membrane permeability cm^2 [1]
Mem.km=0.5*0.39.^1.5*exp(15000/in.R*(1/303.15-1/in.T));  %S/cm [3]
Mem.alpha=Mem.ksatm/in.mu/in.V0.^2*10;% transport coefficient mol^2 / J cm s [3]
Mem.os=2.55*exp(4000/in.R*(1/303.15-1/in.T));% [3]
Mem.beta=0.5.*in.p0vap./(in.ptot-in.p0vap); % Water flux per proton ref 10
Mem.R=Mem.L./Mem.km;


%% Gas Diffusion Layer Characteristics

GDL.L0=0.0235;% compressed from 0.0235(GDL 25 BC properties)
GDL.L=0.0175;  % thickness in cm 

GDL.rho=0.41;%  % porosity (void fraction) ref 8
GDL.ro=[5];%     % characteristic pore radius (um) ref 9
GDL.sr=[0.25];%    % characteristic spread of pore size distribution
GDL.fr=[1];%   % fraction of total pore distribution
% Hydrophobicity Properties
GDL.theta(1)=108*pi/180;      %Ref 6
GDL.fH(1)=55e-02;              % Ref 6 
GDL.fk(1)=1;

% Transport Parameters
% These calculations have been confirmed in previous scripts
GDL.kw=1e-9;% [=]cm2, absolute permiability Ref. 8
GDL.R=0.012;      % ohms-cm2( GDL 25 BC properties)
GDL.keeff=GDL.L./GDL.R;   % Electronic conductivity (effective) S/cm (GDL 25 BC properties)
GDL.ke=7;      % Electronic conductivity Ref. 1




%%

in.Rext=in.Rext0-GDL.R-Mem.R;
in.Rlim=in.Rext0+CL.Ri;
in.GDL=GDL;
in.Mem=Mem;
in.CL=CL;
end %function

% References:
%1.   Weber, A. Z., Darling, R. M., & Newman, J. (2004). Modeling Two-Phase 
%   Behavior in PEFCs. Journal of The Electrochemical Society, 151(10), A1715. 
%   doi:10.1149/1.1792891
%2.   R. B. Bird, W. E. Stewart, and E. N. Lightfoot, Transport Phenomena, 
%   John Wiley
%3.   Weber, A. Z., & Newman, J. (2004). Transport in Polymer-Electrolyte
%   Membranes. Journal of The Electrochemical Society, 151(2), A311. 
%   doi:10.1149/1.1639157
%4.   Siroma, Z., Kakitsubo, R., Fujiwara, N., Ioroi, T., Yamazaki, S., & 
%   Yasuda, K. (2009). Depression of proton conductivity in recast Nafion� 
%   film measured on flat substrate. Journal of Power Sources, 189(2), 
%   994?998. doi:10.1016/j.jpowsour.2008.12.141
%5.   Siroma, Z. (2002). Proton conductivity along interface in thin cast 
%   film of Nafion�. Electrochemistry Communications, 4(2), 143?145. 
%   doi:10.1016/S1388-2481(01)00290-9
%6.   Gostick, J. T., Fowler, M. W., Ioannidis, M. A., Pritzker, M. D., 
%   Volfkovich, Y. M., & Sakars, A. (2006). Capillary pressure and 
%   hydrophilic porosity in gas diffusion layers for polymer electrolyte 
%   fuel cells. Journal of Power Sources, 156(2), 375?387. 
%   doi:10.1016/j.jpowsour.2005.05.086
%7.   Salomov, U. R., Chiavazzo, E., & Asinari, P. (2014). Pore-scale 
%   modeling of fluid flow through gas diffusion and catalyst layers for 
%   high temperature proton exchange membrane (HT-PEM) fuel cells. 
%   Computers & Mathematics with Applications, 67(2), 393?411. 
%   doi:10.1016/j.camwa.2013.08.006
%8.   Schweiss, R., Steeb, M., Wilde, P. M., & Schubert, T. (2012). 
%   Enhancement of proton exchange membrane fuel cell performance by 
%   doping microporous layers of gas diffusion layers with multiwall 
%   carbon nanotubes. Journal of Power Sources, 220, 79?83. 
%   doi:10.1016/j.jpowsour.2012.07.078
%9.   Schweiss, R., Steeb, M., & Wilde, P. M. (2010). Mitigation of Water 
%   Management in PEM Fuel Cell Cathodes by Hydrophilic Wicking 
%   Microporous Layers. Fuel Cells, 10(6), 1176?1180. 
%   doi:10.1002/fuce.201000003
%10.  Janssen, G. J. M. (2001). A Phenomenological Model of Water 
%   Transport in a Proton Exchange Membrane Fuel Cell. Journal of The 
%   Electrochemical Society, 148(12), A1313. doi:10.1149/1.1415031