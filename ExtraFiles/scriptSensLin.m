


%Sensitivity Linearity
figii=0;

%% Sensitivity (Set-up)
tic

in=inputs2B(1,0.45);
in.fig=0;
inair=in;
outnd=PEMFC_NDBVKn(in);
Va=0.8:-0.2:0.4;
percentvalues=logspace(1.5,-1,6);
fpvar=percentvalues./100./2+1;
for ff=1:length(fpvar)
    fp=fpvar(ff);
    fm=2-fp;
    var=[];
    % these are the variables we will study:
    var(1).name='in.CL.fH';
    var(1).title='Hydrophobic Pore Fraction';
    var(1).val=[in.CL.fH(1).*fm in.CL.fH(1) in.CL.fH(1).*fp];
    
    var(2).name='in.CL.rho';
    var(2).title='Catalyst Layer Porosity';
    var(2).val=[in.CL.rho.*fm in.CL.rho in.CL.rho.*fp];
    
    var(3).name='in.CL.kieff';
    var(3).title='Ionic Conductivity / S cm^-^1';
    var(3).val=[in.CL.kieff.*fm in.CL.kieff in.CL.kieff.*fp];
    
    var(4).name='in.CL.keeff';
    var(4).title='Electric Conductivity / S cm^-^1';
    var(4).val=[in.CL.keeff.*fm in.CL.keeff in.CL.keeff.*fp];
    
    var(5).name='in.CL.kw';
    var(5).title='Permeability / cm^2';
    var(5).val=[in.CL.kw.*fm in.CL.kw in.CL.kw.*fp];
    
    var(6).name='in.Rext';
    var(6).title='External Resistance';
    var(6).val=[in.Rext.*fm in.Rext in.Rext.*fp];
    
    var(7).name='in.ioeff';
    var(7).title='Exchange Current Density';
    var(7).val=[in.ioeff.*fm in.ioeff in.ioeff.*fp];
    
    var(8).name='in.CL.L';
    var(8).title='Catalyst Loading';
    var(8).val=[in.CL.L.*fm in.CL.L in.CL.L.*fp];
    
    %% Calculate Air Sensitivity
    for vv=1:length(Va) % for loop through potentials
        in=inair;
        in.pot=Va(vv);
        outnd=PEMFC_NDBVKn(in,outnd);
        outnd0=outnd;
        for vii=1:length(var) % for loop through variables
            for ii=1:3 % -X/2%, Base Value, +X/2%
                
                in=inair;
                %change variables to desired values:
                eval([var(vii).name,' = ones(size(',var(vii).name,')).*',num2str(var(vii).val(ii)),';',]);
                % change potential to desired potential:
                in.pot=Va(vv);
                % if we have changed the porosity, we need to make sure
                % that all the volume fractions add to unity:
                in.CL.vfs=(inair.CL.vfs+inair.CL.rho+inair.CL.vfn)-in.CL.rho-in.CL.vfn;
                
                % run equations
                if ii==2
                    outnd=outnd0;
                else
                    in.pot=Va(vv);
                    outnd=PEMFC_NDBVKn(in,outnd);
                end %if
                
                Iv(vv).I(vii,ii)=outnd.I;
                mod(vii).I(vv,ii)=outnd.I;
                mod(vii).V(vv,ii)=Va(vv);
                % calculate sensitivity:
                if ii>2 %if we have calculated: -X/2%, Base Value, +X/2%
                    senair(vv,vii)=(Iv(vv).I(vii,3)-Iv(vv).I(vii,1))./Iv(vv).I(vii,2)./(fp-fm);
                    linsenair(ff,vii,vv)=senair(vv,vii);
                end %if ii
                
                
            end %for ii
        end %for vii
    end %for vv
    %% Plot Sensitivity
    figii=figii+1;
    figure(figii)
    figlist(figii)={['Air Sensitivity ( ',num2str(percentvalues(ff)),' % Deviation)']};
    
    plot(Va,(senair(:,:)))
    legend(var(1).title,var(2).title,var(3).title,var(4).title,...
        var(5).title,var(6).title,var(7).title,var(8).title)
    ylim([-1 1])
    ylabel('Sensitivity')
    xlabel('Potential / V')
    title(figlist(figii))
    hold off
    
    
    Igorsenair=[Va(:),(senair(:,:))];
    toc
end %for ff

%% Plot Sensitivity as a function of percent deviation
figii=figii+1;
figure(figii)
figlist(figii)={['Air Sensitivity at 0.8 V']};

semilogx(percentvalues,linsenair(:,:,1))
legend(var(1).title,var(2).title,var(3).title,var(4).title,...
    var(5).title,var(6).title,var(7).title,var(8).title)
ylim([-1 1])
ylabel('Sensitivity')
xlabel('Percent Deviation')
title(figlist(figii))
hold off
figii=figii+1;
figure(figii)
figlist(figii)={['Air Sensitivity at 0.4 V']};

semilogx(percentvalues,linsenair(:,:,length(Va)))
legend(var(1).title,var(2).title,var(3).title,var(4).title,...
    var(5).title,var(6).title,var(7).title,var(8).title)
ylim([-1 1])
ylabel('Sensitivity')
xlabel('Percent Deviation')
title(figlist(figii))
hold off

