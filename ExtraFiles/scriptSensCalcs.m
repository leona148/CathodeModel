


%Model Results for Figures in Paper (Figs. 5-8)
figii=0;
%% Polarization Curves (~4 min run time)
findnear= @(v,p) find((v-p).^2==min((v-p).^2));
run1=0;
if run1==1
    tic
    load('MEAdata/Gen2BLot0813varload.mat')
    
%     data(1).V=data1.V;
%     data(2).V=data2.V;
%     data(3).V=data3.V;
%     data(4).V=data4.V;
%     data(1).I=data1.I;
%     data(2).I=data2.I;
%     data(3).I=data3.I;
%     data(4).I=data4.I;
%     mvals=1:4;
    
    
    data(1).V=data2.V;
    data(2).V=data3.V;
    data(3).V=data4.V;
    data(1).I=data2.I;
    data(2).I=data3.I;
    data(3).I=data4.I;
mvals=2:4;
    
    
    psig=[2 12 30];
    pps=1:3;
    pvals=psig(pps)./14.7+0.83;
    
    %P = 30          12         2   psig
    in=inputs2B;
    outnd0=PEMFC_NDBV(in);
    
    vvals=[0.90:-0.01:0.8,0.78:-0.02:0.2];
    load('PolarizationGuess.mat')
    for mm=1:length(mvals)
        
        for pp=1:length(pvals)
            
            outnd=outnd0;
            in=inputs2B(1,0.45,pvals(pp),mvals(mm));
            po(pp)=in.xo.*in.ptot;
            
            for vv=1:length(vvals)
                in=inputs2B(1,0.45,pvals(pp),mvals(mm));
                in.pot=vvals(vv);
                outnd=PEMFC_NDBVf(in,dataout(mm).v(vv,pp).solair);%dataout(mm).v(vv,pp).solair
                toc
                dataout(mm).v(vv,pp).solair=outnd;
                imod(mm).v(vv,pp)=dataout(mm).v(vv,pp).solair.I;
                vmod(mm).v(vv,pp)=vvals(vv);
            end %for vv
            Rmod(mm).v(:,pp)=diff(vmod(mm).v(:,pp))./diff(imod(mm).v(:,pp));
            Rfinmod(mm,pp)=Rmod(mm).v(length(vvals)-1,pp);
            Rfintheo(mm,pp)=in.Rlim;
            Imax(mm,pp)=max(data(mm).I(:,pp));
        end %for pp
    end %for mm
    save('PolarizationGuess.mat','dataout')
    %%
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Polarization with Model'};
    subplot(1,2,1)
    
    plot(data(1).I,data(1).V,'d',...
        data(2).I,data(2).V,'o',data(mm).I,data(mm).V,'s')
    hold on
    plot(imod(1).v,vmod(1).v,...
        imod(2).v,vmod(2).v,imod(mm).v,vmod(mm).v)
    hold off
    legend('1.0 atm 2 mg/cm^2','1.7 atm 2 mg/cm^2','2.9 atm 2 mg/cm^2',...
        '1.0 atm 3 mg/cm^2','1.7 atm 3 mg/cm^2','2.9 atm 3 mg/cm^2',...
        '1.0 atm 4 mg/cm^2','1.7 atm 4 mg/cm^2','2.9 atm 4 mg/cm^2')
    
    xlim([0 2])
    xlabel('Current Density  / A cm^-^2','FontSize',12)
    ylabel('Potential / V','FontSize',12)
    title('Comparison with Experimental Results','FontSize',12)
    
    subplot(1,2,2)
    semilogx(data(1).I,data(1).V,'d',data(2).I,data(2).V,'o',...
        data(mm).I,data(mm).V,'s')
    hold on
    plot(imod(1).v,vmod(1).v,imod(2).v,vmod(2).v,...
        imod(mm).v,vmod(mm).v)
    hold off
    xlim([1e-3 2])
    xlabel('Current Density  / A cm^-^2','FontSize',12)
    ylabel('Potential / V','FontSize',12)
    title('(Gen2B Lot 0630 LM150-OX50)','FontSize',12)
    IgorExp=[data(1).V,data(1).I,data(2).V,data(2).I,...
        data(mm).V,data(mm).I];
    IgorMod=[vmod(1).v,imod(1).v,vmod(2).v,imod(2).v,...
        vmod(mm).v,imod(mm).v];
    toc
    
   
end %if run1


%% Sensitivity (Set-up)
tic

in=inputs2B(1,0.45);
in.fig=0;
inair=in;
in=inputs2B(0,0.45);
in.fig=0;
ino2=in;
outnd=PEMFC_NDBV(in);
Va=0.85:-0.05:0.0;
fp=1.01;
fm=2-fp;
var=[];
var(1).name='in.CL.fH';
var(1).title='Hydrophobic Pore Fraction';
var(1).val=[in.CL.fH(1).*fm in.CL.fH(1) in.CL.fH(1).*fp];

var(2).name='in.CL.rho';
var(2).title='Catalyst Layer Porosity';
var(2).val=[in.CL.rho.*fm in.CL.rho in.CL.rho.*fp];

var(3).name='in.CL.kieff';
var(3).title='Ionic Conductivity / S cm^-^1';
var(3).val=[in.CL.kieff.*fm in.CL.kieff in.CL.kieff.*fp];

var(4).name='in.CL.keeff';
var(4).title='Electric Conductivity / S cm^-^1';
var(4).val=[in.CL.keeff.*fm in.CL.keeff in.CL.keeff.*fp];

var(5).name='in.CL.kw';
var(5).title='Permeability / cm^2';
var(5).val=[in.CL.kw.*fm in.CL.kw in.CL.kw.*fp];

var(6).name='in.Rext';
var(6).title='External Resistance';
var(6).val=[in.Rext.*fm in.Rext in.Rext.*fp];

var(7).name='in.ioeff';
var(7).title='Exchange Current Density';
var(7).val=[in.ioeff.*fm in.ioeff in.ioeff.*fp];

var(8).name='in.CL.L';
var(8).title='Catalyst Loading';
var(8).val=[in.CL.L.*fm in.CL.L in.CL.L.*fp];

%% Air Sensitivity (7 min run time) (potential)
run2=0;
if run2==1
    
    tic
    load('SenAirGuess.mat')
    for vii=1:length(var)
        for ii=1:3
            for vv=1:length(Va)
                in=inair;
                eval([var(vii).name,' = ones(size(',var(vii).name,')).*',num2str(var(vii).val(ii)),';',]);
                in.pot=Va(vv);
                in.CL.vfs=(inair.CL.vfs+inair.CL.rho)-in.CL.rho;
                
                outnd=PEMFC_NDBV(in,outair(vv).outnd);
                toc
                if ii==2
                    outair(vv).outnd=outnd;
                end %if
                
                PIivals(vii,ii)=outnd.CL.PI.i;
                PIevals(vii,ii)=outnd.CL.PI.e;
                PIonvals(vii,ii)=outnd.CL.PI.on;
                Iv(vv).I(vii,ii)=outnd.I;
                mod(vii).I(vv,ii)=outnd.I;
                mod(vii).V(vv,ii)=Va(vv);
                if ii>2
                    senair(vv,vii)=(Iv(vv).I(vii,3)-Iv(vv).I(vii,1))./Iv(vv).I(vii,2)./(fp-fm);
                end %if ii
                
            end %for vv
            
        end %for ii
        
    end %for vii
    save('SenAirGuess.mat','outair')
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Air Sensitivity'};
    
    plot(Va,(senair(:,:)))
    legend(var(1).title,var(2).title,var(3).title,var(4).title,...
        var(5).title,var(6).title,var(7).title,var(8).title)
    ylim([-1 1])
    ylabel('Sensitivity')
    xlabel('Potential / V')
    hold off
    Igorsenair=[Va(:),(senair(:,:))];
    toc
end %if run2
%% Oxygen Sensitivity (6 min run time)
run3=0;
if run3==1
    tic
    in=inputs2B(0,0.45,1);
    in.fig=0;
    in0=in;
    outnd=PEMFC_NDBV(in);
    Va=0.85:-0.05:0.2;
    load('SenO2Guess.mat')
    
    for vii=1:length(var)
        for ii=1:3
            for vv=1:length(Va)
                in=ino2;
                eval([var(vii).name,' = ones(size(',var(vii).name,')).*',num2str(var(vii).val(ii)),';',]);
                in.CL.vfs=(ino2.CL.vfs+ino2.CL.rho)-in.CL.rho;
                
                in.pot=Va(vv);
                outnd=PEMFC_NDBV(in,outo2(vv).outnd);
                if ii==2
                    outo2(vv).outnd=outnd;
                end %if
                toc
                PIivals(vii,ii)=outnd.CL.PI.i;
                PIevals(vii,ii)=outnd.CL.PI.e;
                PIonvals(vii,ii)=outnd.CL.PI.on;
                Iv(vv).I(vii,ii)=outnd.I;
                mod(vii).I(vv,ii)=outnd.I;
                mod(vii).V(vv,ii)=Va(vv);
                if ii>2
                    seno2(vv,vii)=(Iv(vv).I(vii,3)-Iv(vv).I(vii,1))./Iv(vv).I(vii,2)./(fp-fm);
                end %if ii
                
            end %for vv
            
        end %for ii
        
    end %for vii
    save('SenO2Guess.mat','outo2')
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Oxygen Sensitivity'};
    plot(Va,(seno2(:,:)))
    legend(var(1).title,var(2).title,var(3).title,var(4).title,...
        var(5).title,var(6).title,var(7).title,var(8).title)
    ylim([-1 1])
    ylabel('Sensitivity')
    xlabel('Potential / V')
    hold off
    Igorseno2=[Va(:),(seno2(:,:))];
    toc
end %if run3
%% Air Sensitivity (4 min run time) (pressure)
run4=0;
if run4==1
    Pa=[1:0.5:3];
    tic
    load('SenAirGuessP.mat')
    outnd0=PEMFC_NDBV(in);
    for vii=1:length(var)
        for ii=1:3
            for vv=1:length(Pa)
                in=inputs2B(1,0.45,Pa(vv));
                eval([var(vii).name,' = ones(size(',var(vii).name,')).*',num2str(var(vii).val(ii)),';',]);
                in.pot=0.4;
                in.CL.vfs=(inair.CL.vfs+inair.CL.rho)-in.CL.rho;
                
                outnd=PEMFC_NDBV(in,outair(vv).outnd);
                
                
               
                toc
                if ii==2
                    outair(vv).outnd=outnd;
                end %if
                
                PIivals(vii,ii)=outnd.CL.PI.i;
                PIevals(vii,ii)=outnd.CL.PI.e;
                PIonvals(vii,ii)=outnd.CL.PI.on;
                Iv(vv).I(vii,ii)=outnd.I;
                mod(vii).I(vv,ii)=outnd.I;
                mod(vii).V(vv,ii)=Va(vv);
                if ii>2
                    senairp(vv,vii)=(Iv(vv).I(vii,3)-Iv(vv).I(vii,1))./Iv(vv).I(vii,2)./(fp-fm);
                end %if ii
                
                
                in.pot=0.8;
                outnd=PEMFC_NDBV(in,outnd0);
                Iv8(vv).I(vii,ii)=outnd.I;
                if ii>2
                    senairp8(vv,vii)=(Iv8(vv).I(vii,3)-Iv8(vv).I(vii,1))./Iv8(vv).I(vii,2)./(fp-fm);
                end %if ii
                
            end %for vv
            
        end %for ii
        
    end %for vii
    %
    save('SenAirGuessP.mat','outair')
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Air Sensitivity'};
    
    plot(Pa,(senairp(:,:)))
    hold on
    plot(Pa,(senairp8(:,:)),'--')
    legend(var(1).title,var(2).title,var(3).title,var(4).title,...
        var(5).title,var(6).title,var(7).title,var(8).title)
    ylim([-1 1])
    ylabel('Sensitivity')
    xlabel('Pressure / atm')
    hold off
    Igorsenairp=[Pa(:),(senairp(:,:))];
    toc
end %if run2

%% Air Sensitivity (4 min run time) (loading)
run5=1;
if run5==1
    La=[1:0.5:5];
    tic
    load('SenAirGuessP.mat')
    outnd0=PEMFC_NDBV(in);
    for vii=1:length(var)-1
        for ii=1:3
            for vv=1:length(La)
                in=inputs2B(1,0.45,3,La(vv));
                eval([var(vii).name,' = ones(size(',var(vii).name,')).*',num2str(var(vii).val(ii)),';',]);
                in.pot=0.4;
                in.CL.vfs=(inair.CL.vfs+inair.CL.rho)-in.CL.rho;
                
                outnd=PEMFC_NDBV(in,outair(5).outnd);
                
                
               
                toc
                if ii==2
                    outair(vv).outnd=outnd;
                end %if
                
                PIivals(vii,ii)=outnd.CL.PI.i;
                PIevals(vii,ii)=outnd.CL.PI.e;
                PIonvals(vii,ii)=outnd.CL.PI.on;
                Iv(vv).I(vii,ii)=outnd.I;
                mod(vii).I(vv,ii)=outnd.I;
                mod(vii).V(vv,ii)=Va(vv);
                if ii>2
                    senairl4(vv,vii)=(Iv(vv).I(vii,3)-Iv(vv).I(vii,1))./Iv(vv).I(vii,2)./(fp-fm);
                end %if ii
                
                
                in.pot=0.8;
                outnd=PEMFC_NDBV(in,outnd0);
                Iv8(vv).I(vii,ii)=outnd.I;
                if ii>2
                    senairl8(vv,vii)=(Iv8(vv).I(vii,3)-Iv8(vv).I(vii,1))./Iv8(vv).I(vii,2)./(fp-fm);
                end %if ii
                
            end %for vv
            
        end %for ii
        
    end %for vii
    %
    save('SenAirGuessL.mat','outair')
    figii=figii+1;
    figure(figii)
    figlist(figii)={'Air Sensitivity'};
    
    plot(La,(senairl4(:,:)))
    hold on
    plot(La,(senairl8(:,:)),'--')
    legend(var(1).title,var(2).title,var(3).title,var(4).title,...
        var(5).title,var(6).title,var(7).title)
    ylim([-1 1])
    ylabel('Sensitivity')
    xlabel('Pressure / atm')
    hold off
    IgorsenairL=[La(:),(senairl4(:,:))];
    toc
end %if run2